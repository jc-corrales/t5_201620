package taller.mundo;

public class Despachado implements Comparable<Despachado>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Despachado(double pPrecio, String pAutorPedido, int pCerania)
	{
		precio = pPrecio;
		autorPedido = pAutorPedido;
		cercania = pCerania;
	}
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	@Override
	public int compareTo(Despachado elemento)
	{
		if(cercania < elemento.getCercania())
		{
			return 1;
		}
		else if(cercania > elemento.getCercania())
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Método que convierte el elemento despachado en un objeto de la clase pedido.
	 * @return Pedido, elemento convertido.
	 */
	public Pedido toPedido()
	{
		Pedido elemento = new Pedido(precio, autorPedido, cercania);
		return elemento;
	}
	
	// TODO 
}
