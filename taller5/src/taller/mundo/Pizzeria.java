package taller.mundo;

import taller.estructuras.Heap;
import taller.estructuras.IHeap;


public class Pizzeria 
{	
	// ----------------------------------
	// Constantes
	// ----------------------------------

	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap <Pedido> recibidos;
	// TODO 
	/**
	 * Getter de pedidos recibidos
	 */
	public Heap <Pedido> darRecibidos()
	{
		return recibidos;
	}
	// TODO 
	/** 
	 * Heap de elementos por despachar
	 */
	private Heap <Despachado> pendientes;
	// TODO 
	/**
	 * Getter de elementos por despachar
	 */
	public Heap <Despachado> darPendientes()
	{
		return pendientes;
	}
	// TODO 

	// ----------------------------------
	// Constructor
	// ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos = new Heap<Pedido>(0);
		pendientes = new Heap<Despachado>(1);
		// TODO 
	}

	// ----------------------------------
	// Métodos
	// ----------------------------------

	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido nuevo = new Pedido(precio, nombreAutor, cercania);
		recibidos.maxHeap();
		recibidos.add(nuevo);
		recibidos.maxHeap();
		// TODO 
	}

	// Atender al pedido más importante de la cola

	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO
		Pedido siguiente = recibidos.poll();
//		recibidos.minHeap();
		Despachado nuevo = new Despachado(siguiente.getPrecio(), siguiente.getAutorPedido(), siguiente.getCercania());
		pendientes.add(nuevo);
		return  siguiente;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO
		Despachado siguiente = pendientes.poll();
		return  siguiente.toPedido();
	}

	/**
	 * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
	 * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] pedidosRecibidosList()
	{
		Pedido[] lista = new Pedido[recibidos.size()];
		for(int i = 0; i < recibidos.size(); i++)
		{
			lista[i] = recibidos.get(i);
		}
		// TODO 
		return lista;
	}

	/**
	 * Retorna la cola de prioridad de despachos como un arreglo. 
	 * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] colaDespachosList()
	{
		Pedido[] lista = new Pedido[pendientes.size()];
		for(int i = 0; i < pendientes.size(); i++)
		{
			lista[i] = pendientes.get(i).toPedido();
		}
		// TODO 
		return lista;
	}
}
