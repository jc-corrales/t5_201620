package taller.estructuras;

public class Nodo <E> implements Comparable
{
	/**
	 * Atributo del contenido del nodo.
	 */
	private E contenido;
	/**
	 * Atributo que contiene el anterior elemento.
	 */
	private Nodo<E> derecha;
	
	/**
	 * Atributo que contiene el siguiente elemento.
	 */
	private Nodo<E> izquierda;
	
	/**
	 * Constructor del nodo.
	 * @param entry Contenido del nodo de tipo gen�rico.
	 */
	public Nodo(E entry)
	{
		contenido = entry;
		derecha = null;
		izquierda = null;
	}
	/**
	 * M�todo que da el siguiente nodo.
	 * @return
	 */
	public Nodo<E> darDerecho()
	{
		return derecha;
	}
	/**
	 * M�todo que da el anterior nodo.
	 * @return
	 */
	public Nodo<E> darIzquierdo()
	{
		return izquierda;
	}
	/**
	 * Método que da el contenido.
	 * @return
	 */
	public E darContenido()
	{
		return contenido;
	}
	/**
	 * M�todo para cambiar el siguiente Nodo.
	 * @param next
	 */
	public void cambiarDerecho(Nodo<E> next)
	{
		derecha = next;
	}
	
	/**
	 * M�todo para cambiar el anterior Nodo.
	 * @param previous
	 */
	public void cambiarIzquierdo(Nodo<E> previous)
	{
		izquierda = previous;
	}
	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(Object elem)
	{
		return ((Comparable<Object>) contenido).compareTo(elem);
	}
}
