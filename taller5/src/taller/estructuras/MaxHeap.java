package taller.estructuras;

public class MaxHeap<T extends Comparable<T>>
{
	private T[] pq; // heap-ordered complete binary tree
	private int N; // in pq[1..N] with pq[0] unused
	public MaxHeap(T[] array, int size)
	{
		pq = array;
		N = size;
	}
	public boolean isEmpty()
	{
		return N == 0;
	}
	public int size()
	{
		return N;
	}
	public void insert(T v)
	{
		pq[++N] = v;
		swim(N);
	}
	public T delMax()
	{
		T max = pq[0]; // Retrieve max key from top.
		exch(0, N--); // Exchange with last item.
		pq[N+1] = null; // Avoid loitering.
		sink(0); // Restore heap property.
		return max;
	}

	private boolean less(int i, int j)
	{
		return pq[i].compareTo(pq[j]) < 0;
	}

	private void exch(int i, int j)
	{
		T t = pq[i]; pq[i] = pq[j]; pq[j] = t;
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private void sink(int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}
	public T[] orderedArray()
	{
		return pq;
	}
}
