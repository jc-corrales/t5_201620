package taller.estructuras;


public class Heap<T extends Comparable<T>> implements IHeap<T>
{
	//---------------------------------------------------------------------
	//Atributos
	//---------------------------------------------------------------------
	/**
	 * Tamaño actual del heap.
	 */
	private int size;
	/**
	 * Arreglo de clase genérica que contiene los elementos del Heap.
	 */
	private T[] array;
	/**
	 * Algo
	 */
	private T elem;
	
	/**
	 * Entero que determina el sentido del arreglo.
	 */
	private int sentido;
	//---------------------------------------------------------------------
	//Constructor
	//---------------------------------------------------------------------
	/**
	 * Método constructor de la clase. La dirección 0 determina que el mayor va primero, la dirección 1 determina que el menor va primero.
	 * @param direccion int, el valor debe ser entre 0 o 1. Si no es ninguno de los anteriores, el valor del sentido se establecerá como 0.
	 */
	@SuppressWarnings("unchecked")
	public Heap(int direccion)
	{
		array = (T[]) new Comparable[128];
		size = 0;
		if(direccion == 0 || direccion == 1)
		{
			sentido = direccion;
		}
		else
		{
			sentido = 0;
		}
	}
	//---------------------------------------------------------------------
	//Metodos arreglo
	//---------------------------------------------------------------------
	/**
	 * Agrega un elemento al heap
	 */
	public void add(T elemento)
	{
		System.out.println("Add convocado");
		array[size] = elemento;
		size++;	
		if(size == (array.length-1))
		{
			resizeHeap();
		}
		siftUp();
		heapChecker();

		//		Nodo<T> nuevo = new Nodo<T>(elemento);
		//		if(primero == null)
		//		{
		//			primero = new Nodo<T>(elemento);
		//		}
		//		else if(primero.compareTo(elemento) < 0)
		//		{
		//			Nodo<T> temp = primero;
		//			primero = nuevo;
		//			nuevo.cambiarIzquierdo(temp);
		//		}
		//		else
		//		{
		//			
		//		}
		//		
		//		Nodo<T> adecuado = primero;
		//		boolean encontro = false;
		//		while(!encontro)
		//		{
		//			if(adecuado.compareTo(elemento) > 0)
		//			{
		//				if(adecuado.darIzquierdo().compareTo(elemento) < 0 && adecuado.darDerecho().compareTo(elemento) < 0)
		//				{
		//					Nodo<T>temp = adecuado.darIzquierdo();
		//					nuevo.cambiarIzquierdo(temp);
		//					adecuado.cambiarIzquierdo(nuevo);
		//				}
		//				else if(adecuado.darIzquierdo().compareTo(elemento) > 0)
		//				{
		//						
		//				}
		//				else if(adecuado.darDerecho().compareTo(elemento) > 0)
		//				{
		//					
		//				}
		//			}
		//		}
	}

	/**
	 * Retorna pero no remueve el elemento máximo/mínimo del heap.
	 * @return T elemento 
	 */
	public T peek()
	{
		return array[0];
	}

	/**
	 * Retorna el elemento máximo/mínimo luego de removerlo del heap.
	 * @return T El elemento máximo/mínimo del heap
	 */
	public T poll()
	{
		if(array[0] != null)
		{
			T aEliminar = array[0];
			array[0] = null;
			siftDown();
			size--;
			return aEliminar;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Método que retorna el elemento de la posición referenciada, comienza desde 0.
	 * @param int i de posición
	 * @return T
	 */
	public T get(int i)
	{
		return array[i];
	}
	
	/**
	 * Retorna el número de elementos en el heap
	 * @return size Número de elementos en el heap
	 */
	public int size()
	{
		return size;
	}

	/**
	 * Retorna true si el heap no tiene elementos; false de lo contrario.
	 * @return
	 */
	public boolean isEmpty()
	{
		return size == 0;
	}

	/**
	 * Mueve el último elemento arriba en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de inserción.
	 */
	public void siftUp()
	{
		if(sentido == 0)
		{
			for(int i = 0; i < size; i++)
			{
				elem = array[i];
				try
				{
					if((elem.compareTo(array[2*i]) > 0))
					{
						T temp = array[i];
						array[i] = array[2*i];
						array[2*i] = temp;
					}
					else if(elem.compareTo(array[(2*i)+1]) > 0)
					{
						T temp = array[i];
						array[i] = array[(2*i)+1];
						array[(2*i)+1] = temp;
					}
				}
				catch(Exception e)
				{
					continue;
				}
			}
		}
		else
		{
			for(int i = 0; i < size; i++)
			{
				elem = array[i];
				try
				{
					if((elem.compareTo(array[2*i]) < 0))
					{
						T temp = array[i];
						array[i] = array[2*i];
						array[2*i] = temp;
					}
					else if(elem.compareTo(array[(2*i)+1]) < 0)
					{
						T temp = array[i];
						array[i] = array[(2*i)+1];
						array[(2*i)+1] = temp;
					}
				}
				catch(Exception e)
				{
					continue;
				}
			}
		}
	}

	/**
	 * Mueve la raíz abajo en el arbol, mientras que sea necesario.
	 * Es usado para restaurar la condición de heap luego de la eliminación o reemplazo.
	 */
	public void siftDown()
	{
		System.out.println("Siftdown convocado");
		if(sentido == 0)
		{
			for(int i = 0; i < size; i++)
			{
				if(array[i] == null && !isEmpty())
				{
					if(array[2*i] != null)
					{
						array[i] = array[2*i];
						array[2*i] = null;
					}
					else if (array[(2*i)+1] != null)
					{
						array[i] = array[(2*i)+1];
						array[(2*i)+1] = null;
					}
					else
					{
						array[i] = array[i+1];
						array[i+1] = null;
					}
				}
				else if(!isEmpty())
				{
					try
					{
						if((elem.compareTo(array[2*i]) >= 0))
						{
							T temp = array[i];
							array[i] = array[2*i];
							array[2*i] = temp;
						}
						else if(elem.compareTo(array[(2*i)+1]) >= 0)
						{
							T temp = array[i];
							array[i] = array[(2*i)+1];
							array[(2*i)+1] = temp;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
			}
			siftUp();
		}
		else if(sentido == 1)
		{
			for(int i = 0; i < size; i++)
			{
				if(array[i] == null && !isEmpty())
				{
					if(array[2*i] != null)
					{
						array[i] = array[2*i];
						array[2*i] = null;
					}
					else if (array[(2*i)+1] != null)
					{
						array[i] = array[(2*i)+1];
						array[(2*i)+1] = null;
					}
					else
					{
						array[i] = array[i+1];
						array[i+1] = null;
					}
				}
				else if(!isEmpty())
				{
					try
					{
						if((elem.compareTo(array[2*i]) <= 0))
						{
							T temp = array[i];
							array[i] = array[2*i];
							array[2*i] = temp;
						}
						else if(elem.compareTo(array[(2*i)+1]) <= 0)
						{
							T temp = array[i];
							array[i] = array[(2*i)+1];
							array[(2*i)+1] = temp;
						}
					}
					catch(Exception e)
					{
						continue;
					}
				}
			}
			siftUp();
		}
	}

	/**
	 * Método que ordena los objectos de mayor a menor.
	 */
	public void maxHeap()
	{
		MaxHeap<T> ordenador = new MaxHeap<T>(array, size);
		array = (T[]) ordenador.orderedArray();
//		sort(array, 0);
//		heapChecker();
//		for(int i = 1; i < size; i++)
//		{
//			if(i%2 == 1)
//			{
//				if(array[i+1] != null)
//				{
//					T temp = array[i];
//					array[i] = array[i+1];
//					array[i+1] = temp;
//				}
//			}
//		}
	}
	/**
	 * Método que ordena los elementos de menor a mayor.
	 */
	public void minHeap()
	{
//		sort(array, 1);
	}

	public int compareTo(Heap<T> o)
	{
		return getT().compareTo(o.getT());
	}

	/**
	 * Método que retorna t;
	 * @return T
	 */
	public T getT()
	{
		return elem;
	}
	
	/**
	 * Método que retorna el arreglo completo.
	 * @return T[]
	 */
	public T[] darContenido()
	{
		return array;
	}
	/**
	 * Método para cambiar el sentido del ordenamiento.
	 * @param criterio int 0 o 1.
	 */
	public void cambiarSentido(int criterio)
	{
		if(criterio == 0 || criterio == 1)
		{
			sentido = criterio;
			heapChecker();
		}
	}
	/**
	 * Método que modifica el tamaño del arreglo en caso de que el heap se llene.
	 */
	@SuppressWarnings("unchecked")
	private void resizeHeap()
	{
		T[] nuevo = (T[]) new Object[array.length*2];
		for(int i = 0; i <= array.length; i++)
		{
			try
			{
				if(array[i] != null)
				{
					nuevo[i] = array[i];
				}
				else
				{
					break;
				}
			}
			catch(Exception e)
			{
				break;
			}
		}
	}
	
	/**
	 * Método que revisa que la condición Heap esté mantenida.
	 */
	private void heapChecker()
	{
		int contador = 0;
		for(int i = 0; i < size; i++)
		{
			try
			{
				if(array[2*i] != null)
				{
					if(array[i].compareTo(array[2*i]) > 0)
					{
						contador++;
					}
				}
				if(array[(2*i)+1] != null)
				{
					if(array[i].compareTo(array[(2*i)+1]) > 0)
					{
						contador++;
					}
				}
			}
			catch (Exception e)
			{
				continue;
			}
		}
		int j = 0;
		while(j <= contador)
		{
			siftUp();
			j++;
		}
	}
	//---------------------------------------------------------------------
	//Métodos algoritmo de ordenamiento, método reciclado del taller 4.
	//---------------------------------------------------------------------
	
//	/**
//	 * Método que organiza el array.
//	 * @param list Array
//	 * @param sentido int, en donde es 0 o 1.
//	 * @return array ordenado.
//	 */
//	private Comparable[] sort(Comparable[] list, int sentido)
//	{
//		Comparable[] lista = merge_sort(list);
//		if(sentido == 1)
//		{
//			inversor(lista);
//		}
//		return lista;
//	}
//	/**
//	 * Método que permite la comparación en el ordenamiento.
//	 * @param lista
//	 * @return
//	 */
//	private static Comparable[] merge_sort(Comparable[] lista)
//	{
//		Comparable[] aux = new Comparable[lista.length];
//		sort(lista, aux, 0, lista.length - 1);
//
//		return lista;
//	}
//	/**
//	 * 
//	 * @param lista
//	 * @param aux
//	 * @param lo
//	 * @param hi
//	 */
//	private static void sort(Comparable[] lista, Comparable[] aux, int lo, int hi)
//	{
//		if (hi <= lo) return;
//		int mid = lo + (hi - lo) / 2;
//		sort(lista, aux, lo, mid);
//		sort(lista, aux, mid+1, hi);
//		merge(lista, aux, lo, mid, hi); 
//	}
//	/**
//	 * @param lista
//	 * @param aux
//	 * @param lo
//	 * @param mid
//	 * @param hi
//	 */
//	private static void merge(Comparable[] lista, Comparable[] aux, int lo, int mid, int hi )
//	{
//		for (int k = lo; k <= hi; k++)
//			aux[k] = lista[k];
//		int i = lo, j = mid+1;
//		for (int k = lo; k <= hi; k++)
//		{
//			if (i > mid) lista[k] = aux[j++];
//			else if (j > hi) lista[k] = aux[i++];
//			else if (aux[j].compareTo(aux[i]) < 0) lista[k] = aux[j++];
//			else lista[k] = aux[i++];
//		} 
//	}
//	
//	/**
//	 * Método que invierte el contenido de la lista.
//	 * @param list lista a invertir.
//	 */
//	private void inversor(Comparable[] list)
//	{
//		for(int j = list.length-1, i = 0; i < list.length && i<j; i++, j--)
//		{
//			Comparable temp = list[i];
//			list[i] = list[j];
//			list[j] = temp;
//		}
//	}
	
	
	
	
	
//	private T[] pq; // heap-ordered complete binary tree
//	private int N;
//	public void sort()
//	{
//		pq = array;
//		N = size();
//	}
//	
//
//	
//	private void insert(T v)
//	{
//		pq[++N] = v;
//		swim(N);
//	}
//	private T delMax()
//	{
//		T max = pq[1]; // Retrieve max key from top.
//		exch(1, N--); // Exchange with last item.
//		pq[N+1] = null; // Avoid loitering.
//		sink(1); // Restore heap property.
//		return max;
//	}
//	public static void sort(Comparable[] a)
//	{
//		int N = a.length;
//		for (int k = N/2; k >= 1; k--)
//			sink(a, k, N);
//		while (N > 1)
//		{
//			exch(a, 1, N--);
//			sink(a, 1, N);
//		}
//	}

//	private void sink(int k)
//	{
//		while (2*k <= size)
//		{
//			int j = 2*k;
//			if (j < size && less(j, j+1)) j++;
//			if (!less(k, j)) break;
//			exch(k, j);
//			k = j;
//		}
//	}
//
//	private void swim(int k)
//	{
//		while (k > 1 && less(k/2, k))
//		{
//			exch(k/2, k);
//			k = k/2;
//		}
//	}
//
//	private boolean less(int i, int j)
//	{
//		return pq[i].compareTo(pq[j]) < 0;
//	}
//	private void exch(int i, int j)
//	{
//		T t = pq[i]; pq[i] = pq[j]; pq[j] = t;
//	}
}
